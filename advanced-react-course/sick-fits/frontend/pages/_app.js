import { ApolloProvider } from 'react-apollo';
import App, { Container } from 'next/app';
import Page from '../components/Page';
import withData from '../lib/withData';

// Loaded on every page
class MyApp extends App { // not extending React.Component, but App from next
	static async getInitialProps({ Component, ctx }) { // getInitialProps is from next.js, is run before render
		let pageProps = {};
		if (Component.getInitialProps) {
			pageProps = await Component.getInitialProps(ctx);
		}
		// this exposes the query to the user
		pageProps.query = ctx.query;
		return { pageProps }; // returning in getInitialProps exposes it to this.props
	}

	render() {
		// Includes the components to load on current page (for example, receives 'Sell' component (from pages/Sell.js) when on url /sell)
		const { Component, apollo, pageProps } = this.props;

		return (
			<Container>
				<ApolloProvider client={apollo}>
					<Page>
						<Component {...pageProps} />
					</Page>
				</ApolloProvider>
			</Container>
		)
	}
}

export default withData(MyApp);
