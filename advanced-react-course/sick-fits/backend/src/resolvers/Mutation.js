const Mutations = {
	async createItem(parent, args, ctxt, info) {
		// TODO: check if they are logged in

		const item = await ctxt.db.mutation.createItem({
			data: {
				...args
			}
		}, info);

		console.log(item);

		return item;
	},

	updateItem(parent, args, ctxt, info) {
		// first take a copy of the updates
		const updates = { ...args }; 
		// remove ID from the updates
		delete updates.id;
		// run the update method
		return ctxt.db.mutation.updateItem({
			data: updates,
			where: {
				id: args.id
			}
		}, info);
	},

	async deleteItem(parent, args, ctxt, info) {
		const where = { id: args.id };
		// find the item
		const item = await ctxt.db.query.item({ where }, `{ id title }`);
		// check if they own that item or have the permissions
		// TODO: ...
		// delete it
		return ctxt.db.mutation.deleteItem({ where }, info);
	}
};

module.exports = Mutations;
