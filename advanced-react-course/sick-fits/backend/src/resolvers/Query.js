const { forwardTo } = require('prisma-binding');

const Query = {
	// when the request is exactly the same as the prisma request (so no extra checks etc), it can be forwarded to prisma directly
	items: forwardTo('db'),
	item: forwardTo('db'),
	itemsConnection: forwardTo('db'),
	
	// async items(parent, args, ctxt, info) {
	// 	const items = await ctxt.db.query.items();
	// 	return items;
	// }
};

module.exports = Query;
